package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer>{
	@Query("select t from Task t where t.limit_date between ?1 and ?2 and t.content like %?3% and t.status = ?4 order by id")
	List<Task> findByTime(Timestamp start, Timestamp end, String content, int status);

	@Query("select t from Task t where t.limit_date between ?1 and ?2 and t.content like %?3% order by id")
	List<Task> findByTimeAll(Timestamp start, Timestamp end, String content);
}