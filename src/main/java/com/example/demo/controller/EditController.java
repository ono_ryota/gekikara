package com.example.demo.controller;

import java.net.http.HttpRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Task;
import com.example.demo.service.EditService;

@Controller
public class EditController {

	@Autowired
	EditService editService;

//	@Autowired
//	HttpRequest request;

	//タスク編集画面表示
	@GetMapping("/edit/{id}")
	public ModelAndView editTask(@PathVariable String id, RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView();

		//idが数字以外もしくは存在しないタスクidを取得したらエラー処理
		if(id.matches("^[0-9]*$") && editService.findById(Integer.parseInt(id)) != null) {
			Task task = editService.findById(Integer.parseInt(id));

			String limitDateString = editService.createStringLimitDate(task);

			task.setLimit_date_and_time(limitDateString);
			mav.setViewName("edit");
			mav.addObject("formModel", task);
		} else {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("不正なパラメータです");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			return new ModelAndView("redirect:/");
		}

		return mav;
	}

	//タスク編集画面のURLで末尾にidが入力されていなかった場合のエラー処理
	@GetMapping("/edit")
	public ModelAndView editTaskIdError(RedirectAttributes redirectAttributes) {
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータです");
		redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
		return new ModelAndView("redirect:/");
	}

	//タスク編集処理
	@PutMapping("/task/update/{id}")
	public ModelAndView updateTask(@PathVariable Integer id, @Validated @ModelAttribute("formModel") Task task,
			BindingResult error) {

		List<String> errorMessages = new ArrayList<String>();
		String limitDateAndTime = task.getLimit_date_and_time();
		Timestamp limitDate = null;

		//期限が入力されていなかった場合のエラー処理
		if(!editService.checkInputLimit(limitDateAndTime)) {
			FieldError inputLimitDateError = new FieldError(error.getObjectName(), "limit_date_and_time", "期限を入力してください");
			error.addError(inputLimitDateError);
		} else {
			limitDate = editService.createLimitDate(limitDateAndTime);
			//現在より前の期限が入力された場合のエラー処理
			if(!editService.checkLimitDate(limitDate) || limitDate == null) {
				FieldError limitDateError = new FieldError(error.getObjectName(), "limit_date_and_time", "無効な日付です");
				error.addError(limitDateError);
			}
		}

		//全角スペースのみ入力されたときのエラー処理
		//NotBlankは全角スペースに対応していないため実装
		if(!editService.fullWidthSpace(task.getContent())) {
			FieldError contentError = new FieldError(error.getObjectName(), "content", "タスクを入力してください");
			error.addError(contentError);
		}

		if(error.hasErrors()) {
			for(ObjectError result : error.getAllErrors()) {
				errorMessages.add(result.getDefaultMessage());
			}

			ModelAndView mav = new ModelAndView();
			mav.addObject("formModel", task);
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("edit");
			return mav;
		}

		task.setLimit_date(limitDate);
		editService.save(task);
		return new ModelAndView("redirect:/");
	}
}