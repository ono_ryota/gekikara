package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.AddService;
@Controller
public class AddController {
	@Autowired
	AddService addService;

	// タスク追加画面表示
	@GetMapping("/add")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		// form用の空のentityを準備
		Task task = new Task();

		mav.setViewName("/add");

		// 準備した空のentityを保管
		mav.addObject("formModel",task);
		return mav;
	}

	// タスク追加処理
	@PostMapping("/add")
	public ModelAndView addContent(@Validated @ModelAttribute("formModel") Task task, BindingResult error) {

		List<String> errorMessages = new ArrayList<String>();
		String limitDateAndTime = task.getLimit_date_and_time();
		Timestamp limitDate = null;

		//期限が未入力だった場合のエラー処理
		if(task.getLimit_date_and_time().isBlank()) {
			FieldError limitDateBlankError = new FieldError(error.getObjectName(),"limit_date_and_time","期限を入力してください");
			error.addError(limitDateBlankError);
		}else {
			limitDate = addService.createLimitDate(limitDateAndTime);

			//現在日時より前の日時を入力した場合のエラー処理
			if(!addService.checkLimitDate(limitDate)) {
				FieldError LimitDatePastError = new FieldError(error.getObjectName(),"limit_date_and_time","無効な日付です");
				error.addError(LimitDatePastError);
			}
		}

		if(task.getContent().matches("^　+$")) {
			FieldError SpaceError = new FieldError(error.getObjectName(),"content","タスクを入力してください");
			error.addError(SpaceError);
		}

		if(error.hasErrors()) {
			for(ObjectError result : error.getAllErrors()) {
				errorMessages.add(result.getDefaultMessage());
			}
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/add");
			return mav;
		}

		task.setLimit_date(limitDate);
		addService.saveTask(task);
		return new ModelAndView("redirect:/");
	}
}
