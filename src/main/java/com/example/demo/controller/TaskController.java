package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@Controller
public class TaskController{

	@Autowired
	TaskService taskService;

	// TOP画面表示
	@GetMapping
	public ModelAndView top(){
		ModelAndView mav = new ModelAndView();
		// タスクの取得
		List<Task> taskData = taskService.findAllTask();
		// 現在時刻の取得
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		// 画面遷移先の指定
		mav.setViewName("/top");
		// オブジェクトを保管
		mav.addObject("tasks", taskData);
		mav.addObject("currentTime", currentTime);

		//TOP画面へフォワード
		return mav;
	}

	// タスク削除
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteTask(@PathVariable Integer id){
		// タスクをテーブルから削除
		taskService.deleteTask(id);
		// TOP画面へリダイレクト
		return new ModelAndView("redirect:/");
	}

	// ステータス変更
	@GetMapping("/change/{id}")
	public ModelAndView changeStatus(@PathVariable Integer id){
		ModelAndView mav = new ModelAndView();
		// idのタスクを取得
		Task task = taskService.selectTask(id);
		// ステータスの変更(3種類なので3の剰余)
		task.setStatus((task.getStatus() + 1) % 3);
		// タスクのステータスを変更
		taskService.changeStatus(task);
		// オブジェクトを保管
		mav.addObject("formModel", task);
		// TOP画面へリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 絞り込み
	@GetMapping("search")
	public ModelAndView searchTask(@RequestParam("start") String start, @RequestParam("end") String end,
			@RequestParam("content") String content, @RequestParam("status") String status){
		ModelAndView mav = new ModelAndView();
		// タスクの絞り込み
		List<Task> taskData = taskService.searchTask(start, end, content, status);
		// 現在時刻の取得
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		// 画面遷移先の指定
		mav.setViewName("/top");
		// オブジェクトを保管
		mav.addObject("tasks", taskData);
		mav.addObject("currentTime", currentTime);
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.addObject("content", content);
		mav.addObject("status", status);

		//TOP画面へフォワード
		return mav;
	}

}
