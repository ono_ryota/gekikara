package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class TaskService{

	@Autowired
	TaskRepository taskRepository;

	// レコードをid順に全件取得
	public List<Task> findAllTask(){
		return taskRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
	}

	// レコードを削除
	public void deleteTask(Integer id){
		taskRepository.deleteById(id);
	}

	// レコードのステータスを更新
	public void changeStatus(Task task){
		taskRepository.save(task);
	}

	// レコードの絞り込み
	public List<Task> searchTask(String start, String end, String content, String status){
		if(StringUtils.hasText(start)){
			start = start + " 00:00:00";
		}else{
			start = "2020-01-01 00:00:00";
		}

		if(StringUtils.hasText(end)){
			end = end + " 23:59:59";
		}else{
			end = "2100-12-31 23:59:59";
		}
		Timestamp startTime = Timestamp.valueOf(start.replace("T", " "));
		Timestamp endTime = Timestamp.valueOf(end.replace("T", " "));

		if(status == ""){
			status = null;
			List<Task> taskData = taskRepository.findByTimeAll(startTime, endTime, content);
			return taskData;
		}else{
			Integer statusInt = Integer.valueOf(status);
			List<Task> taskData = taskRepository.findByTime(startTime, endTime, content, statusInt);
			return taskData;
		}
	}

	// レコードを1件取得
	public Task selectTask(Integer id){
		Task task = (Task) taskRepository.findById(id).orElse(null);
		return task;
	}
}
