package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class EditService {

	@Autowired
	TaskRepository taskRepository;

	//編集画面表示のためのID取得メソッド
	public Task findById(Integer id) {
		return taskRepository.findById(id).orElse(null);
	}

	//タスク更新メソッド
	public void save(Task task) {
		taskRepository.save(task);
	}

	//期限が現在日時より前の日時かチェックするメソッド
	public boolean checkLimitDate(Timestamp limitDate) {

		Date date = new Date();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String currentDateString = dateformat.format(date) + ":00";
		Timestamp currentDate = Timestamp.valueOf(currentDateString);

		if(currentDate.after(limitDate)) {
			return false;
		} else {
			return true;
		}

	}

	//期限が入力されたかチェックするメソッド
	public boolean checkInputLimit(String limitDateAndTime) {
		if(StringUtils.hasText(limitDateAndTime)) {
			return true;
		} else {
			return false;
		}
	}

	//入力された期限をタイムスタンプ型に変換するメソッド
	public Timestamp createLimitDate(String limitDateAndTime) {

		String inputLimitDateAndTime = limitDateAndTime;
		String datetimelocal = null;
		if(StringUtils.hasText(inputLimitDateAndTime)) {
			datetimelocal = inputLimitDateAndTime.replace("T", " ");
			datetimelocal = datetimelocal + ":00";
		} else {

		}

		Timestamp limitData = Timestamp.valueOf(datetimelocal);
		return limitData;
	}

	//編集画面に遷移したときに期限を表示させるためのメソッド
	public String createStringLimitDate(Task task) {
		Timestamp limitDate = task.getLimit_date();
		String limitDateString = String.valueOf(limitDate);
		limitDateString = limitDateString.replace(" ", "T");
		StringBuilder sb = new StringBuilder(limitDateString);
		sb = sb.delete(16, 21);
		limitDateString = sb.toString();

		return limitDateString;
	}

	//
	public boolean fullWidthSpace(String taskContent) {

		if(taskContent.matches("^　+$")) {
			return false;
		} else {
			return true;
		}
	}
}
