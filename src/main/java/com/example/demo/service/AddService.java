package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class AddService {
	@Autowired
	TaskRepository taskRepository;

	public void saveTask(Task task) {
		taskRepository.save(task);
	}

	//入力された期限が現在時刻より前か判断する処理
	public boolean checkLimitDate(Timestamp limitDate) {
		Date date = new Date();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String currentDateString = dateformat.format(date) + ":00";
		Timestamp currentDate = Timestamp.valueOf(currentDateString);

		if(currentDate.after(limitDate)) {
			return false;
		} else {
			return true;
		}

	}

	//入力された期限をタイムスタンプ型に変換するメソッド
	public Timestamp createLimitDate(String limitDateAndTime) {

		String inputLimitDateAndTime = limitDateAndTime;
		String datetimelocal = null;
		if(StringUtils.hasText(inputLimitDateAndTime)) {
			datetimelocal = inputLimitDateAndTime.replace("T", " ");
			datetimelocal = datetimelocal + ":00";
		} else {

		}

		Timestamp limitData = Timestamp.valueOf(datetimelocal);
		return limitData;
	}
}
